/**
 * * Copyright 2017 yarl.
 * *
 * * This file is part of rouille.
 * *
 * * rouille is free software: you can redistribute it and/or modify
 * * it under the terms of the GNU General Public License as published by
 * * the Free Software Foundation, either version 3 of the License, or
 * * (at your option) any later version.
 * *
 * * rouille is distributed in the hope that it will be useful,
 * * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * * GNU General Public License for more details.
 * *
 * * You should have received a copy of the GNU General Public License
 * * along with rouille.  If not, see <http://www.gnu.org/licenses/>.
 * */

extern crate systemd;
extern crate scgi;
extern crate bufstream;

use systemd::daemon::*;
use std::error::Error;
use std::os::unix::net::{UnixStream, UnixListener};
use std::os::unix::io::FromRawFd;
use std::collections::BTreeMap;
use std::io::Write;
use std::string::String;
use std::thread;
use std::fmt::{self, Display, Formatter};
use std::collections::HashMap;
use std::sync::Arc;
use std::ops::{Deref,DerefMut};

//implement display on an http field
macro_rules! disp {
    ($t:ty, $a:expr, { $( $x:pat => $y:expr, )* }) => {
impl Display for $t {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", $a)?;
        let s = match *self {
            $( $x => $y, )*
        };
        write!(f, "{}\r\n", s)
    }
}
    };
}

fn prereq() -> Result<(),Box<Error>> {
    if !(booted()?) {
        return Err(From::from("Not booted by systemd"));
    }
    let a = listen_fds(true)?;
    if a != 1 {
        return Err(From::from(format!("listen_fds == {}", a)));
    }   
    if !is_socket(LISTEN_FDS_START, None, Some(SocketType::Stream), Listening::IsListening)? {
        return Err(From::from("Socket is not stream or not listening"));
    }
    Ok(())
}

enum Status {
    StatusOk,
    NotFound,
}
use Status::*;
disp!(Status, "Status: ", {
    StatusOk => "200 Ok",
    NotFound => "404 Not Found",
});

enum ContentType {
    TextPlain,
    TextEventStream,
}
use ContentType::*;
disp!(ContentType, "Content-Type: ", {
    TextPlain => "text/plain",
    TextEventStream => "text/event-stream",
});

enum CacheControl {
    NoCache,
}
use CacheControl::*;
disp!(CacheControl, "Cache-Control: ", {
    NoCache => "no-cache",
});

trait Field: Display {}
impl Field for ContentType {}
impl Field for CacheControl {}

struct HttpHeader(Status, Vec<Box<Field>>);
impl Display for HttpHeader {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", &self.0)?;
        for field in &self.1 {
            write!(f, "{}", field)?;
        }
        write!(f, "\r\n")
    }
}

struct PageHandler {
    header: String,
    handler: Option<Box<Fn(bufstream::BufStream<UnixStream>, BTreeMap<&str,&str>)->Result<(), Box<Error>> + Sync + Send>>,
}
impl PageHandler {
    fn new(_header: String, 
           _handler: Option<Box<Fn(bufstream::BufStream<UnixStream>, BTreeMap<&str,&str>)->
                Result<(), Box<Error>> + Sync + Send>>) ->
    PageHandler {
        PageHandler{header: _header, handler: _handler}
    }
    fn write(&self, mut bs: bufstream::BufStream<UnixStream>, t: BTreeMap<&str,&str>) -> Result<(), Box<Error>>  {
        write!(bs, "{}", self.header)?;
        if let Some(ref handler) = self.handler {
            handler(bs, t)
        } else {
            Ok(())
        }
    }
}

struct PageHandlers<'a>
{
    inner: HashMap<&'a str, PageHandler>,
}
impl<'a> PageHandlers<'a> {
    fn new() -> PageHandlers<'a> {
        PageHandlers {inner: HashMap::new()}
    }
}
impl<'a> Deref for PageHandlers<'a> {
    type Target = HashMap<&'a str, PageHandler>;
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}
impl<'a> DerefMut for PageHandlers<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

fn handle_client(stream: UnixStream, 
                 handlers: Arc<PageHandlers>) 
        -> Result<(), Box<Error>> {
    let (v, bs) = scgi::read_headers(stream)?;
    let t: BTreeMap<&str, &str> = scgi::str_map(&v[..])?;
    println!("{:?}", t);
    let uri: &str = t.get("REQUEST_URI").ok_or::<Box<Error>>(From::from("No REQUEST_URI header"))?;
    let ph = handlers.get(uri).unwrap_or_else(||handlers.get("404").unwrap());
    ph.write(bs,t)
}

fn main() {
    prereq().unwrap();
    let hokplain = format!("{}", HttpHeader(StatusOk, vec![Box::from(TextPlain)]));
    let notfound = format!("{}", HttpHeader(NotFound, vec![]));
    let hello_process = |mut bs: bufstream::BufStream<UnixStream>, _: BTreeMap<&str,&str>| {
        write!(bs, "Hello\r\n").map_err(Box::<Error>::from)
    };
    let goodbye_process = |mut bs: bufstream::BufStream<UnixStream>, _: BTreeMap<&str,&str>| {
        write!(bs, "Goodbye\r\n").map_err(Box::<Error>::from)
    };
    let mut handlers = PageHandlers::new();
    handlers.insert("/hello.scgi", PageHandler::new(hokplain.clone(), Some(Box::new(hello_process))));
    handlers.insert("/goodbye.scgi", PageHandler::new(hokplain.clone(), Some(Box::new(goodbye_process))));
    handlers.insert("404", PageHandler::new(notfound.clone(), None));
    let handlers = Arc::new(handlers);
    let listener = unsafe { UnixListener::from_raw_fd(LISTEN_FDS_START) };
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection");
                let h = handlers.clone();
                thread::spawn(move || {
                    match handle_client(stream, h) {
                        Ok(_) => (),
                        Err(e) => println!("Error: {:?}", e),
                    }
                });
            },
            Err(err) => println!("Connection failed: {:?}", err),
        }
    }
}

